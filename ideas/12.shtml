<!--#include virtual="/webtechdemo/showcase/_includes-static/head.shtml"-->

<div class="layout layout--twocol-25-75">

  <div  class="layout__region layout__region--top">
    <div class="block block--page-title-block">
      <h1 class="page-title">Idea #12: Using Open Data to Build Research Skills</h1>
    </div>
  </div>

  <div  class="layout__region layout__region--first">
    <!--#include virtual="navigation.shtml"-->
  </div>

  <div  class="layout__region layout__region--second">
    <p><em>“We argue that OD [open data] used as OERs, when supported by a critical pedagogy scaffolding, would represent a powerful resource for learners.”</em><sup>3</sup></p>

    <h2>Learning Intention</h2>

    <p>“These datasets can be used as Open Educational Resources (OER) to support different teaching and learning activities, allowing students to gain experience working with the same raw data researchers and policy-makers generate and use. In this way, educators can facilitate students to understand how information is generated processed, analyzed, and interpreted.”<sup>1</sup> Generally, open data can be used pedagogically for students to create new research through analysis of data and build critical thinking skills, enabling students to become engaged citizens.</p>

    <h2>Overview</h2>

    <p>Organizations, NGOs, academic research institutions, and governments are making data increasingly available. Open data includes “volumes of structured and unstructured data from various fields. The large datasets produced represent a wide--and only partially explored--range of opportunities in many fields, including science, medicine, engineering, social services and education, to mention just a few.”3 Educators can teach research-based learning by asking students to pose research questions, test these questions with qualitative and quantitative techniques with open data, and present their findings.</p>

    <h2>Benefits to Students</h2>

    <ol>
      <li>
        <p><strong>Critical Thinking:</strong> “Curricula can enable students to engage more frequently with the needs of society through critical engagement with raw data.”<sup>1</sup></p>
      </li>
      <li>
        <p><strong>Data Curation Skills:</strong> Provide students with knowledge of data curation, organization, storage, and analysis.</p>
      </li>
      <li>
        <p><strong>Research Skills:</strong> Students can use data to conduct new and original research even as undergraduates.</p>
      </li>
      <li>
        <p><strong>Statistical Literacies:</strong> In order to analyze large amounts of data, students must have skills for analyzing statistics. Students can use open data to generate statistical inquiries.</p>
      </li>
      <li>
        <p><strong>Global Citizenship: </strong>Open data can promote enduring understandings of global citizenship by forcing students to be aware of local and global problems which analysis of the data may reveal.</p>
      </li>
    </ol>

    <h2>Considerations</h2>

    <ul>
      <li>
        <p><strong>Address data ethics: </strong>More and more large companies and organizations are analyzing data to gain knowledge about consumers. Consider structuring a unit around data ethics and how we collect, organize, and interpret data ethically.</p>
      </li>
      <li>
        <p><strong>Reframe student understanding of the world around them through data: </strong>“By extracting narratives from OD, we [can] connect students directly with what is happening in their historical and social context so that they can better face the real challenges of the twenty-first century” (p. 113)<sup>3</sup>.</p>
      </li>
      <li>
        <p><strong>Have students work with local data:</strong> Having students work with data local to the community where they live can be a way for them to connect prior knowledge to the course and connect their learning to policy decisions being made in the community. “The rationale for using open data was to shift the focus towards an outward-looking approach to... engage students in constructing applications that had real-world relevance.”<sup>2</sup></p>
      </li>
      <li>
        <p><strong>Have students meet with local policy makers:</strong> If students are working with local open data, consider having students meet with the governments, groups, or organizations that might benefit from their research.</p>
      </li>
    </ul>

    <h2>References</h2>

    <ol>
      <li>
        <p>Atenas, J., Havemann, L. &amp; Priego, E. (2015). Open Data as Open Educational Resources: Towards Transversal Skills and Global Citizenship. Open Praxis, 7(4), 377-389. International Council for Open and Distance Education. Retrieved May 24, 2019 from <a href="https://www.learntechlib.org/p/161986/">https://www.learntechlib.org/p/161986/</a></p>
      </li>
      <li>
        <p>Atenas, J., &amp; Havemann, L. (Eds.). (2015). Open Data as Open Educational Resources: Case studies of emerging practice. London: Open Knowledge, Open Education Working Group. Available at: <a href="http://dx.doi.org/10.6084/m9.figshare.1590031">http://dx.doi.org/10.6084/m9.figshare.1590031</a></p>
      </li>
      <li>
        <p>Manca, A., Atenas, J., Ciociola, C., Nascimbeni, F. (2017) Critical pedagogy and open data for education towards social cohesion.<em> Italian Journal of Educational Technology, 25</em>(1). Available at: <a href="https://doi.org/10/17471/2499-4324/862">https://doi.org/10/17471/2499-4324/862</a></p>
      </li>
    </ol>


  </div>

</div>

<!--#include virtual="/webtechdemo/showcase/_includes-static/foot.shtml"-->
