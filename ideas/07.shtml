<!--#include virtual="/webtechdemo/showcase/_includes-static/head.shtml"-->

<div class="layout layout--twocol-25-75">

  <div  class="layout__region layout__region--top">
    <div class="block block--page-title-block">
      <h1 class="page-title">Idea #7: From Disposable to Renewable Assignments</h1>
    </div>
  </div>

  <div  class="layout__region layout__region--first">
    <!--#include virtual="navigation.shtml"-->
  </div>

  <div  class="layout__region layout__region--second">
    <p><em>“Students are publishing for an audience greater than their instructor. That matters. Their work, being open, has the potential to be used for something larger than the course itself and to be part of a larger global conversation.”</em><sup>2</sup></p>

    <h2>Learning Intention</h2>

    <p>“In order to increase engagement, to promote self-directed and self-regulated learning as well as collaboration and knowledge sharing, and to encourage the development of products of value beyond the grading process, there is a need to expand our vision of educational content so that the greater value is placed on student-created products as a primary content source.”<sup><em>4</em></sup></p>

    <h2>Overview</h2>

    <p>“Although student-generated content has been a part of the education system for many decades, its role has been highly marginalized and its forms restricted to highly academic artifacts used exclusively for assessment purposes, such as essays and reports.”<sup><em>4</em></sup><em> </em>Non-disposable, or renewable, assignments serve a purpose beyond grading for an audience beyond the instructor.</p>

    <h2>Examples<sup><em>4</em></sup></h2>

    <ul>
      <li>
        <p>Have students develop a research project with a community partner. The research they are able to complete can then be used by the community partner to benefit the local community.</p>
      </li>
      <li>
        <p>Have students write opinion pieces that share evidence-based approaches to a local social problem.</p>
      </li>
      <li>
        <p>Have students help curate the course itself. Have them find and share research of their own that relates to course themes.</p>
      </li>
      <li>
        <p>Have students speak with experts. Bring experts into the classroom that are local to the community or it could be online. Student can easily engage and interact with experts through social media platforms like Twitter or video conferencing like Google Hangouts or Zoom.<sup><em>7</em></sup></p>
      </li>
      <li>
        <p>Build OERs with students or have them edit/remix existing OERs.</p>
      </li>
    </ul>

    <h2>Considerations</h2>

    <ul>
      <li>
        <p>Clearly align with student learning or course objectives.</p>
      </li>
      <li>
        <p>Need to provide impact of value outside of grades.</p>
      </li>
      <li>
        <p>Involve information collaboration and exchange to “strengthen higher-order media literacy skills to push student learning horizons beyond the old limitations of time and space.”<em>3</em></p>
      </li>
      <li>
        <p>Involve communication and opportunity for revision, creativity, modification.</p>
      </li>
      <li>
        <p>Share outside of teacher-student relationship.</p>
      </li>
      <li>
        <p>Produce learning through cooperative critique.</p>
      </li>
    </ul>

    <h2>References</h2>

    <ol>
      <li>
        <p>DOER Fellows Renewable Assignments. Retrieved from <a href="https://openedgroup.org/doer-fellows-renewable-assignments">https://openedgroup.org/doer-fellows-renewable-assignments</a></p>
      </li>
      <li>
        <p>Nascimbeni, F., &amp; Burgos, D. (2016). In Search for the Open Educator: Proposal of a Definition and a Framework to Increase Openness Adoption Among University Educators. <em>The International Review of Research in Open and Distributed Learning</em>, <em>17</em>(6). https://doi.org/10.19173/irrodl.v17i6.2736</p>
      </li>
      <li>
        <p>McLoughlin, C., &amp; Lee, M.J. (2010). Personalised and self regulated learning in the Web 2.0 era: International exemplars of innovative pedagogy using social software. <em>Australasian Journal of Educational Technology, 26</em>(1), 28-43. Available at: <a href="http://etec.ctlt.ubc.ca/510wiki/images/5/54/49023651.pdf">http://etec.ctlt.ubc.ca/510wiki/images/5/54/49023651.pdf</a></p>
      </li>
      <li>
        <p>O’Reilly, J. (2018). Day 14 - Renewable Assignments. Retrieved from <a href="https://www.open.edu/openlearncreate/mod/page/view.php?id=138718">https://www.open.edu/openlearncreate/mod/page/view.php?id=138718</a></p>
      </li>
      <li>
        <p>Jhangiani, R. &amp; DeRosa, R. Open Pedagogy Notebook: Sharing Practices, Building Community. Retrieved on May 30, 2019, from <a href="http://openpedagogy.org/">http://openpedagogy.org/</a></p>
      </li>
      <li>
        <p>Seraphin, S.B., Grizzell, A.J., Kerr-German, A., Perkins., M.A., Granka, P.R., &amp; Hardin, E.E. (2019). A Conceptual Framework for Non-Disposable Assignments: Inspiring Implementation, Innovation, and Research. <em>Psychology Learning &amp; Teaching, 18</em>(1), 84-97. DOI: 10.1177/1475725718811711</p>
      </li>
      <li>
        <p>Staton, M. (2019). Creating Lifelong Learners through Open Educational Practices. Innovative Teaching Showcase. Published online by the Center for Instructional Innovation and Assessment, Western Washington University.</p>
      </li>
    </ol>

    <p>Wiley, D. (2015). An Obstacle to the Ubiquitous Adoption of OER in US Higher Education. Retrieved from <a href="https://opencontent.org/blog/archives/3941">https://opencontent.org/blog/archives/3941</a></p>


  </div>

</div>

<!--#include virtual="/webtechdemo/showcase/_includes-static/foot.shtml"-->
