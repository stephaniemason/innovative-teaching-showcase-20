<!--#include virtual="/webtechdemo/showcase/_includes-static/head.shtml"-->

<div class="layout layout--twocol-25-75">

  <div  class="layout__region layout__region--top">
    <div class="block block--page-title-block">
      <span class="pre-title">Portfolio</span>
      <h1 class="page-title">Kamarie Chapman</h1>
    </div>
  </div>

  <div  class="layout__region layout__region--first">
    <!--#include virtual="../navigation.shtml"-->
    <!--#include virtual="navigation.shtml"-->
  </div>

  <div  class="layout__region layout__region--second">
    <div class="block block--testimonial square">
      <div class="image image--stylized">
        <img src="/webtechdemo/showcase/_source/images/chapman_sm.jpg" alt="Kamarie Chapman">

      </div>
      <div class="body">
        <h2>Reel to Reality: Opening the Curriculum to Teach Beyond Technique in a Modern GUR Class</h2>
      </div>

      <div class="credit">
        <span class="name">Kamarie Chapman </span>
        <span class="description">Department of Theatre & Dance</span>
      </div>
    </div>

    <h3 id="reaching-students">Reaching Students</h3>
    <img src="/webtechdemo/showcase/featured/chapman/images/chapman_teaching.jpg" class="right" alt="Chapman teaching">

    <p>Instructing a large GUR film class at a mid-sized, liberal arts college means instructing a diverse representation of students; students who are diverse culturally, socioeconomically, and in terms of abilities. These factors and challenges have persuaded my pedagogy with young adults greatly, especially the accessibility issues. Coming from a background of teaching puppetry and acting to adults with developmental disabilities, it has always been a top priority for me to ensure all participants are given an equal opportunity to succeed in my courses—even if that success may look a little different from a more traditional model. Because of this focus, over the last nine years, it has become extremely clear that accessibility is an issue that ALL students face—whether it’s cultural representation in the works studied, being able to make the print larger in a text or to download it for a Braille-Lite computer, or being able to afford a text that (no matter how interesting) will inevitably end up in a fifty-cent pile at a garage sale someday. And so, the stage is set to figure out a way to educate the students with relevant information that they can afford, identify with, and manipulate as needed for accessibility. </p>

    <div class="block block--pullquote pullquote--editorial left">
      <blockquote>
        <div class="body">
          Because of this focus, over the last nine years, it has become extremely clear that accessibility is an issue that ALL students face—whether it’s cultural representation in the works studied, being able to make the print larger in a text or to download it for a Braille-Lite computer, or being able to afford a text that (no matter how interesting) will inevitably end up in a fifty-cent pile at a garage sale someday.
        </div>
      </blockquote>
    </div>

    <p>A reasonable thought would be to utilize the online access that so many text companies offer now, and that seemed to be the right answer at first. However, textbook companies have proven to not be proficient in supporting student technologies. They don’t really understand that class participants are not sitting in campus computer labs doing their homework. They think it’s acceptable for the professors to dictate how they need to access the materials when the fact is, they are accessing the text from any possible device they can and not necessarily from a platform that we understand. Many of them are using proprietary software or software to translate the English to another language such as Japanese, Korean, Arabic, etc. The price drop isn’t significant enough to entice students to go to the computer labs or access only from approved devices. This knowledge comes from two years of trying to work with woefully inadequate publishers who are good at creating useful texts, but terrible at technical support.</p>

    <p>This was just one publishing company; possibly the fatigue of constantly trying to appease students facing frustrating technical difficulties drove my decision to change to using open source materials, but something else equally (if not more) significant happened.</p>

    <h3 id="the-catalyst">The Catalyst</h3>

    <img src="/webtechdemo/showcase/featured/chapman/images/metoo.jpg" class="left" alt="Magazine letters cutout to read #metoo">

    <p>The winter of 2017 actor Alyssa Milano tweeted, “If you’ve been sexually harassed or assaulted write ‘me too’ as a reply to this tweet.” Within 48 hours of this tweet, # became an international creation of community and a movement in fourth-wave feminism. When examining the etymology of “Me too,” it’s important to note that Tarana Burke founded the movement and coined the phrase in 2006, as a trailblazing term for women and girls of color who were survivors of sexual abuse. But when Hollywood actress, Alyssa Milano, used it in a tweet when the allegations against Harvey Weinstein started flooding the media, then it happened… Every day there seemed to be a new story, a new survivor. There had been allegations against men who had been known as heroes in all industries, but this time people were listening. This time, people were saying that it is important to believe the survivors <em>first</em>.</p>

    <p>Perhaps it is the fact that feeling brave and taking chances is part of #metoo, or perhaps my exhaustion from teaching a very large GUR for five years set in, or perhaps it was just time to shake things up—but I very quickly began to realize it was time to cut ties with a textbook (or the idea of a textbook) and engage students in a different way.</p>

    <div class="block block--pullquote pullquote--editorial left">
      <blockquote>
        <div class="body">
          ...perhaps it was just time to shake things up—but I very quickly began to realize it was time to cut ties with a textbook (or the idea of a textbook) and engage students in a different way.
        </div>
      </blockquote>
    </div>

    <img src="/webtechdemo/showcase/featured/chapman/images/The_Breakfast_Club.jpg" class="right" alt="The Breakfast Club movie poster">

    <p>Really, the person that sparked the idea was Molly Ringwald. </p>

    <p>Yes. Molly Ringwald of <em>Sixteen Candles, Breakfast Club, Pretty in Pink—</em>the fiery red-head with the pouty lips featured in films by John Hughes and Prom Queen of The Brat Pack. Turns out, she is quite the prolific writer and had a lot to say about the misogyny, nostalgia, and how #metoo has completely changed the film industry and this ambrosia so many cinephiles buy into; especially academics and old Hollywood Fellows. (See “<a href="https://www.newyorker.com/culture/personal-history/what-about-the-breakfast-club-molly-ringwald-metoo-john-hughes-pretty-in-pink">What About the Breakfast Club?</a>” in the New Yorker)</p>

    <p>It was this article that was the catalyst for my personal process in which a file was created on an old laptop to stash away all the articles that began to discuss film, cinema, and Hollywood in a different way. Not all of them were so blatant in terms of the message as Ms. Ringwald’s article, but many of them discussed feature films and the lack of diversity, not only on the screen, but from writers, directors, and actors as well. Also, I found articles discussing film creation in a completely different way than traditionally taught. Ever.</p>

    <p>And it made sense.</p>

    <h3 id="filling-the-void">Filling the Void</h3>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman-class5.jpg" class="left" alt="Chapman in front of a black and white movie clip of two Japanese men">

    <p>There are no textbooks about film written by women, people of color, or other non-traditional voices. Why? Because textbooks are written by academics, <em>tenured</em> academics who teach film. Tenured academics are not generally included in the aforementioned community. For the most part, they are white, affluent men; men who came from affluent families and had the privilege of going to nice colleges. Many of these were prestigious film colleges that they helped found, because they were the generation that created the system, or at least their dads were. Many of them had short-lived careers where maybe they were not the most successful directors or writers, but they were successful in understanding the population that created film and its aesthetic and they could write about that and get publishing deals.</p>

    <div class="block block--pullquote pullquote--editorial right">
      <blockquote>
        <div class="body">
          It is a systemic problem; one with which many of us (especially those of us who studied the arts) are familiar. We learn from those who learned from those who wrote the books about those who created the films and wrote the history; a white history.
        </div>
      </blockquote>
    </div>

    <p>It is a systemic problem; one with which many of us (especially those of us who studied the arts) are familiar. We learn from those who learned from those who wrote the books about those who created the films and wrote the history; a white history. In film, this is a white history that praises <em>Citizen Kane, The Cabinet of Dr. Caligari, </em>and <em>Butch Cassidy and the Sundance Kid.</em> Indeed, these are stunning films. However, how does one relate to a very large group of mostly first-year, non-major students that these are the foundations of cinema and what they should appreciate? It is very hard, even with an engaging, fun text that has lots of video supplementals. Having tried for five years, my conclusion was that it just doesn’t work.</p>

    <p>How can I teach film in a better, more universal, relevant way that makes sense for liberal arts and this iGen of students? I was obsessed for months with this question.</p>

    <p>Back to the stash of articles… now dozens and dozens of articles specifically written by non-traditional writers. The result: film CAN be taught another way. An understanding of what film is and how to teach and appreciate was borne.</p>


    <h3 id="journey-ahead">The Journey Ahead</h3>

    <div class="block block--pullquote pullquote--editorial left">
      <blockquote>
        <div class="body">
          This is the part where things got scary—I started realizing that the way in which I taught film, and was taught, and my mentors were probably taught was not relevant or necessarily good.
        </div>
      </blockquote>
    </div>

    <p>This is the part where things got scary—I started realizing that the way in which I taught film, and was taught, and my mentors were probably taught was not relevant or necessarily good. It certainly is one way, and the only way, things had been done. The path was clear and well-traveled: first week (cultural relevance), second week (history), third week (editing and continuity), fourth (<em>Mise-en-scène)</em>, etc. Even typing this now, I can hear the song “Tradition!” from <em>Fiddler on the Roof</em> run through my thoughts. Great. Now that’s a thing. You are welcome.</p>

    <img src="/webtechdemo/showcase/featured/chapman/images/Fiddler_On_the_Roof.jpg" class="right" alt="Group of performers on stage in old timey clothes">

    <p>I started to think about my course and what information I needed and wanted students to take with them, how that was applicable to life outside the university, and what students can take with them as they progress through their course of study at the university.</p>

    <p>The strangest thing about all this is how it applied to other classes I teach. I had been teaching this way (open access and relatable script drafts) for the playwrights and dramaturgs for years. I have also had a remarkable history of having learned those skills from a pretty equal blend of traditional and non-traditional playwrights and screenwriters as well as historians and practitioners in the performing arts; a fact now that I am just realizing is extremely rare.</p>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman-class1.jpg" class="left" alt="Two students gazing up smiling">

    <p>Why would applying these practices to the large GUR be so much different? Why does there need to be a traditional quiz each week, and an essay, midterm, and final exam? Those have never been methods that spoke to me as an educator in the first place.</p>

    <p>Back to the dangerous and scary waters… it was clear that the time had come to toss out the old ways and come up with my own— lessons that make sense to me and these students. Reaching out to WWU’s Center for Instructional Innovation (where I had learned five years previously how to blend my courses and use online resources to make the time in-class with the students more meaningful), I applied for a grant to help me replace the text for this GUR class with open-access and openly licensed materials. I already had nearly enough articles, short tutorials, podcasts, etc. that could generate enough material to replace a text. These materials better represented the students in my class, humans from all different backgrounds working on all kinds of different artistic paths. This was really exciting to me, especially because the participants could use whatever device they wanted to access the materials because they are all online. Frankly, it’s the best way for them to have access. We don’t need to monitor what devices they are using or how it’s being translated because they have already been doing this (a possibility not even fathomable ten years ago).</p>

    <p>Remodeling the idea of how to teach an introductory cinema course was critical. After researching and rereading all the articles, tutorials, and other random sources, I had to find a way to interpret how these points-of-view worked together. If having the students read the sources directly was the plan, then they need a path to connect the ideas. I developed a map of the way I was understanding this to all work. See <a href="/webtechdemo/showcase/featured/chapman/docs/THTR201-Big-Ideas.pdf">Big Ideas</a>.</p>

    <div class="block block--pullquote pullquote--editorial">
      <blockquote style="margin: 48px auto;">
        <div class="body">
          Remodeling the idea of how to teach an introductory cinema course was critical. After researching and rereading all the articles, tutorials, and other random sources, I had to find a way to interpret how these points-of-view worked together.
        </div>
      </blockquote>
    </div>

    <h3 id="the-new-plan">The New Plan</h3>

    <div class="block block--pullquote pullquote--editorial right">
      <blockquote>
        <div class="body">
          The first thing to do was decide: <em>WHY offer this course?</em> What is the reason that a four-year, liberal arts university wants to have this course taught and is an option for all students to take for Humanities credit? Honestly, it is a bit overwhelming, but that is the way to really figure out what needs to be done.
        </div>
      </blockquote>
    </div>

    <p>The first thing to do was decide: <em>WHY offer this course?</em> What is the reason that a four-year, liberal arts university wants to have this course taught and is an option for all students to take for Humanities credit? Honestly, it is a bit overwhelming, but that is the way to really figure out what needs to be done. I decided that what it comes down to is: <em>Catharsis is a human need.</em> (Ta-da!) Great. So now to break that down into sections that make sense with the source materials I have gathered. This university teaches in quarters, so in general, there are ten weeks each quarter to convey the information, which is why there are five main sections of the class. Those five sections are:</p>

    <ol>
      <li>Shared Stories</li>
      <li>Structure</li>
      <li>Empathy</li>
      <li>Diversity of Voice</li>
      <li>Technique</li>
    </ol>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman-class2.jpg" class="left" alt="A grinning, talking student">

    <p>This is the skeleton of the course. The storyboard that can really show off the structure before we clutter it up with all kinds of interesting articles and tutorials (for this metaphor, the conflicts). This gave me the guidance to spend about two weeks in each of these sections, each of which can be moved or divided around as needed because they all fold into one another. That means for this model of the course, the ten weeks break down like this:</p>

    <h4>Week One: Historical Parallels</h4>

    <ul>
      <li>What is German Expressionism, Spanish Surrealism, Mexican Golden Cinema?</li>
      <li>How was film the best portrayal of this form of resistance?</li>
      <li>Why is it relevant now and who is most influenced by it?</li>
    </ul>

    <h4>Week Two: Growing up on the Silver Screen</h4>

    <ul>
      <li>Why are the “Coming of Age” films so important?</li>
      <li>What about genre? How does it define a generation/culture/group?</li>
    </ul>

    <h4>Week Three: Feeling the Feelings We Feel (Catharsis)</h4>

    <ul>
      <li>Why are some films so moving?</li>
      <li>What tricks are used to suck us in? (Structure/Design)</li>
      <li>What defines a “good” film?</li>
    </ul>

    <h4>Week Four: Story, Narrative, &amp; Plot (Oh my!)</h4>

    <ul>
      <li>How do we tell stories? Is there a formula or a rule?</li>
      <li>Is the most important part of the story HOW it is told?</li>
    </ul>

    <h4>Week Five: Documentary</h4>

    <ul>
      <li>Is film truth? Cinema Verité vs. Direct Cinema</li>
      <li>Children’s Programming</li>
    </ul>

    <h4>Week Six: The New Turks (Mod)</h4>

    <ul>
      <li>Editing and breaking structures/rules</li>
      <li>Experimental Film</li>
      <li>Women as Directors (FINALLY)</li>
    </ul>

    <h4>Week Seven: Samurai Cinema</h4>

    <ul>
      <li>Why does cinematography matter?</li>
      <li>The art of beauty (as defined through a lens)</li>
    </ul>

    <h4>Week Eight: Cell Phone Cinema</h4>

    <ul>
      <li>How to make a film on your phone with free apps</li>
      <li>Technology leveling the playing field</li>
    </ul>

    <h4>Week Nine: Appropriation vs. Inspired By</h4>

    <ul>
      <li>The best steal from the best</li>
      <li>When do we acknowledge the inspiration?</li>
      <li>What is the artist’s duty to humanity?</li>
    </ul>

    <h4>Week Ten: The Audience</h4>

    <ul>
      <li>What is the responsibility and expectation of the audience?</li>
      <li>How do you choose films? How do you watch them?</li>
      <li>It’s okay to just be entertained, right?</li>
    </ul>

    <h3 id="how-it-works">How It Works</h3>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman-class3.jpg" class="right" alt="A contemplative looking student next to another student writing on an ipad">

    <p>Sometimes these sections get moved (depending on the term and the breaks and holidays). Often, I have the students work on the cellphone cinema section at home. Sometimes I record them a couple extra lectures to go with it. But in general, what happens is they get a more well-rounded understanding of cinema and why we as humans need it.</p>

    <p>The quizzes are taken weekly together (with notes, sources, and help from partners) after the viewing of that week’s film. The questions are designed to get them trying to apply methods and theories to the script or techniques of that week’s filmmaker. This seems to have had absolutely zero change in the grades. Everything still rests on a pretty solid curve, which is strange; I really thought it would be a lot more bloated at the top, but the students that are generally above average regulate themselves into their own groups, and those that do not do the work are too embarrassed to try and join a group. This has also been hugely helpful for students that do not speak English as their native language.</p>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman-class4.jpg" class="left" alt="A group of smiling students in a lecture hall">

    <p>But, does it work? The final project of this course requires them to do an individual film review as a vlog or as a group, create a short documentary or scene. The quality of these productions has gone up immensely. The students feel empowered by understanding the devices they have utilized in their created films. They learn about sourcing and research through the example I give them in terms of how the materials are created for the class, but also through the required sourcing of their references with the final product. See <a href="/webtechdemo/showcase/featured/chapman/docs/THTR201-Student-Project-Selections.pdf">Student Project Selections</a>.</p>

    <blockquote>
      <p><em>I want to thank you for an amazing class. I never have seen myself as a "Movie Buff", but I have really enjoyed the movies that have been shown in class. I have also loved the videos and articles you have given us each week. It is so awesome that you teach us to see movies in a different way; and have us really think about the choices they make to convey their message.</em></p>
      <p style="text-align: right;">—Student in Cinema 201, Spring 2019</p>
    </blockquote>

    <h3 id="next-level">Next Level</h3>

    <p>Ultimately, a dream would be for faculty to all have access to open sources across the curriculum, and to engage and challenge our own ways of teaching by regularly researching and engaging with the material we enjoy teaching.</p>

    <p>One day, I’d like to see all this hard work culminated for individual students into a working online presence. One that holds their final essays, art projects, speeches, performances, lab results, etc. in an individualized website. A resource that students could use to share with potential employers, grad schools, or internships. Something that tracks their learning so that five (or ten or twenty) years after graduation they can see the value of this four-year education and have access to all the sources that transpired. And ultimately, they could pass this understanding of research down to the next generations. They could create a new structure and new ideas that reflect the values of all we’ve come to understand so far—values of common knowledge, multicultural voice, and accessible resources for all that can open a dialogue for all generations of educators and students alike.</p>

    <img src="/webtechdemo/showcase/featured/chapman/images/chapman_teaching2.jpg" alt="Chapman standing in front of a lecture hall of students" style="width: 100%;">

  </div>

  <div  class="layout__region layout__region--bottom">
  </div>
</div>

<!--#include virtual="/webtechdemo/showcase/_includes-static/foot.shtml"-->
