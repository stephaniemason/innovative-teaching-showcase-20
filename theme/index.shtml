<!--#include virtual="/webtechdemo/showcase/_includes-static/head.shtml"-->

<div class="layout layout--onecol">

  <div class="layout__region">
    <div class="block block--page-title-block">
      <span class="pre-title">Theme</span>
      <h1 class="page-title">From OER to Open Pedagogy: Harnessing the Power of Open</h1>
    </div>

    <img src="/webtechdemo/showcase/_source/images/robin-and-scott.png" alt="Portraits of Robin DeRosa and Scott Robison" style="float: right; width: 40%; margin-left: 24px">
    <p>Robin DeRosa (Plymouth State University) and Scott Robison (Portland State University)</p>

    <p>Excerpted with grateful attribution to the authors, as permitted by Creative Commons License: CC-BY 4.0 from Open: The Philosophy and Practices that are Revolutionizing Education and Science.</p>

    <h2>Editors’ Commentary </h2>
    <p>The unaffordability of education—whether in terms of tuition or textbooks—has undoubtedly made the ‘free’ element of Open a rallying cry. However, as the open education movement matures, the fulcrum of this discussion appears to be shifting from an emphasis on the adoption of open educational resources to an embrace of open educational practices… </p>


    <div class="video-container" style="position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden;"><iframe width="853" height="480" src="https://www.youtube.com/embed/-FIvvxRkqFs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

    <h2>Understanding the Value of Open</h2>
    <p>There is no question that Open Educational Resources can save students money, and there is no question that the cost of higher education can be prohibitive for many students, so lowering costs is a shared imperative for those of us who are committed to educational access. But lowering costs always has to be contextualized into larger goals about learning. … So what about OER make them a good choice for adoption in the classroom? What, aside from cost-savings, make them valuable to education? </p>

    <img src="/webtechdemo/showcase/_source/images/inflation-graph.png" alt="inflation rate compared to textbooks over the period of 2010-2018. Two upward trend lines, one for texbooks showing 41% increase and one for inflation showing 17% increase." style="float: left; margin: 24px; width: 50%;">

    <p>OER are free, digital, easily shared learning materials. Though colleges will have to address hardware issues (it would be a mistake not to acknowledge that access to the online world has real costs, and that free materials can only be freely available when institutions assure provision for all students), the potential that OER have to lower skyrocketing textbook costs is promising. When you look at the majority of research and press about OER, they focus on the rising costs of textbooks and the phenomenal cost-saving potential of OER. Individual students could save thousands of dollars over the course of an academic degree; colleges and universities could save hundreds of thousands — even millions — for their student bodies. In addition, institutions stand to strengthen their own financial health as they improve retention and enrollment rates by committing to OER initiatives. So, should faculty convert to OER because it’s cheaper for students…or our institutions? …or is something larger at stake here?</p>

    <div class="block block--pullquote pullquote--editorial">
      <blockquote style="margin: 48px auto">
        <div class="body">
          "Open Educational Resources(OER) are teaching, learning, and research resources that reside in the public domain or have been released under an intellectual property license that permits their free use and re-purposing by others."
        </div>
        <div class="credit">
          <span class="name"> Hewlett Foundation</span>
        </div>
      </blockquote>
    </div>

    <p>First, we need a corrective to the definition in the previous paragraph, since it’s not enough to say that ‘OER are free, digital, easily shared learning materials.’ To be ‘open’ as well as free, educational materials must carry an open license (usually a Creative Commons license), meaning that OER can be reused, remixed, revised, redistributed, and retained. In other words,</p>

    <blockquote>
      <p>OER are flexible, and they empower faculty and students to work together to customize learning materials to suit specific courses and objectives. </p>
    </blockquote>

    <aside>
      <h2>The 5Rs of OER</h2>
      <img src="/webtechdemo/showcase/_source/images/5rs.png" alt="5R logo: Reuse, Revise, Remix, Redistribute, Retain" style="float: right; width: 40%">

      <p>Open Educational Resources (OER) provide teachers, learners, and others with legal permissions to engage in the 5R activities.</p>

      <ol>
        <li>Reuse: Content can be reused in its unaltered form</li>
        <li>Retain: Users have the right to make, archive, and "own" copies of the content</li>
        <li>Revise: content can be adapted, adjusted, modified, or altered</li>
        <li>Remix: the original content can be combined with other content to create something new</li>
        <li>Redistribute: Copies of the content can be shared with others in its original revised or remixed form</li>
      </ol>
    </aside>

    <p>It’s the way that the learning materials respond to learners and teachers that makes OER exciting; what should really galvanize faculty with an interest in educational transformation are the possibilities for pedagogical change that OER make explicit. </p>

    <p>By replacing a static textbook — or other stable learning material — with one that is openly licensed, faculty have the opportunity to create a new relationship between learners and the information they access in the course. </p>

    <blockquote>
      <p>Instead of thinking of knowledge as something students need to download into their brains, we start thinking of knowledge as something continuously created and revised.</p>
    </blockquote>

    <p>Whether students participate in the development and revision of OER or not, this redefined relationship between students and their course ‘texts’ is central to the philosophy of learning that the course espouses. If faculty involve their students in interacting with OER, this relationship becomes even more explicit, as students are expected to critique and contribute to the body of knowledge from which they are learning. In this sense, knowledge is less a product that has distinct beginning and end points and is instead a process in which students can engage, ideally beyond the bounds of the course. </p>

    <div class="block block--pullquote pullquote--editorial">
      <blockquote style="margin: 48px auto">
        <div class="body">
          "Building on open education resources (OER), open educational practices seek to fully use the potential inherent in OER to support learning and to help students both contribute to knowledge and construct their own learning pathways. <strong>Such open practices provide the architecture and philisophical underpinning for fulfilling the promise of using OER to expand collaborative, inclusive, accessible, and active learning related pedagogy.</strong> Open educational practices also give agency to sutdents by giving them more control over the structure, content, and outcomes of their learning and by creating opportunities for them to create learning materials."
        </div>
        <div class="credit">
          <span class="name"> 7 Things You SHould Know About Open Educational Practices</span>
          <span class="desc">Educause</span>
        </div>
      </blockquote>
    </div>

    <p>If texts — content — are at the heart of a course, and if content is now shaped into a process that depends on learner engagement in order to function fully, then OER propel us into truly student-centered territory. This territory might more aptly be described as ‘learner-centered’ or even ‘learner-directed’ if we follow through on the open pedagogy towards which OER gesture. (For the purposes of this inquiry, this article defines ‘open pedagogy’ in a way that remixes and revises the complex definition of ‘critical digital pedagogy’ set forth by Jesse Stommel.) </p>

    <p>OER make possible the shift from a primarily student-content interaction to an arrangement where the content is integral to the student-student and student-instructor interactions as well. </p>

    <blockquote>
      <p>What we once thought of as pedagogical accompaniments to content (class discussion, students assignments, etc.) are now inextricable from the content itself, which has been set in motion as a process by the community that interacts with it. </p>
    </blockquote>

    <p>Moreover, students asked to interact with OER become part of a wider public of developers, much like an open-source community. We can capitalize on this relationship between enrolled students and a broader public by drawing in wider communities of learners and expertise to help our students find relevance in their work, situate their ideas into key contexts, and contribute to the public good. We can ask our students — and ourselves as faculty — not just to deliver excellence within a prescribed set of parameters, but to help develop those parameters by asking questions about what problems need to be solved, what ideas need to be explored, what new paths should be carved based on the diverse perspectives at the table.</p>

    <blockquote>
      <p>Open pedagogy uses OER as a jumping-off point for remaking our courses so that they become not just repositories for content, but platforms for learning, collaboration, and engagement with the world outside the classroom.</p>
    </blockquote>

    <h2>The Power of Open: A concluding thought </h2>

    <p>If we think of OER as just free digital stuff, as products, we can surely lower costs for students; we might even help them pass more courses because they will have free, portable, and permanent access to their learning materials. But we largely miss out on the opportunity to empower our students, to help them see content as something they can curate and create, and to help them see themselves as contributing members to the public marketplace of ideas. </p>

    <blockquote>
      <p>Essentially, this is a move from thinking about OER as open textbooks and thinking about them as opening textbooks...and all sorts of other educational materials and processes. When we think about OER as something we do rather than something we find/ adopt/acquire, we begin to tap their full potential for learning.</p>
    </blockquote>

    <img src="/webtechdemo/showcase/_source/images/open-door-to-universe.png" alt="A door opened to a painting of the universe">

    <h2>Citation</h2>
    <p>DeRosa, R. and Robison S. 2017. From OER to Open Pedagogy: Harnessing the Power of Open. In: Jhangiani, R S and Biswas-Diener, R. (eds.) Open: The Philosophy and Practices that are Revolutionizing Education and Science. Pp. 115–124. London: Ubiquity Press. DOI: <a href="https://doi.org/10.5334/bbc.i">https://doi.org/10.5334/bbc.i</a>. License: <a href="https://creativecommons.org/licenses/by/4.0/deed.ast">CC-BY 4.0</a></p>

    <h2>See also:</h2>
    <a href="http://robinderosa.net/">Robin DeRosa’s Blog</a>

    <h2>Online Access to the Full Version</h2>
    <a href="https://www.ubiquitypress.com/site/books/10.5334/bbc/">Open: The Philosophy and Practices that are Revolutionizing Education and Science</a>

  </div>

</div>

<!--#include virtual="/webtechdemo/showcase/_includes-static/foot.shtml"-->
