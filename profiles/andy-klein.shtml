<div class="block block--testimonial">
  <div class="image image--stylized">
    <img src="/webtechdemo/showcase/_source/images/Klein_Andy.jpg" alt="Andy Klein">

  </div>
  <div class="body">
    <h2 id="andy-klein">Andy Klein</h2>
    <p class="tagline">Department of Engineering and Design</p>

    <p>Andy Klein applies an Open Educational Practices philosophy to his courses EE 360 and EE 460. Having been frustrated with the traditional electrical engineering textbooks that were heavy in theory but lacking in practical application, Andy co-authored and published the free textbook <a href="https://cnx.org/contents/QsVBJjB4@3.1:0itCPCIA@4/A-Digital-Radio"><em>Software Receiver Design: How to Build Your Own Communication System in Five </em></a></p>

    <p><a href="https://cnx.org/contents/QsVBJjB4@3.1:0itCPCIA@4/A-Digital-Radio"><em>Easy Steps</em></a> through Cambridge University Press. It is currently hosted through OpenSTAX and is the required text for both of the classes he teaches.</p>

  </div>
</div>

<h3>EE 360: Communication Systems</h3>

<ul>
  <li>
    <p>This course introduces analog and digital communication systems with an emphasis on system level concepts.</p>
  </li>
  <li>
    <p>Some of the fundamental principles that are covered are: modulation and demodulation, radio architectures and receiver structures, bandwidth requirements, SNR, pulse shaping, and synchronization.</p>
  </li>
</ul>

<h3>EE 460: Digital Communication Systems</h3>

<ul>
  <li>
    <p>This course focuses on advanced digital communication system design, analysis, and implementation.</p>
  </li>
  <li>
    <p>Random processes will be used to model communication systems and analyze their performance in noise.</p>
  </li>
  <li>
    <p>Advanced communication techniques will be studied, such as: multicarrier systems, spread spectrum, equalization, channel coding, and modern wireless protocols and standards.</p>
  </li>
</ul>
